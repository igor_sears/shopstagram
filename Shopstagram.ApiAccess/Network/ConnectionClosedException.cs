﻿using System;

namespace Shopstagram.ApiAccess.Network
{
	internal class ConnectionClosedException : Exception
	{
		public ConnectionClosedException(string message) : base(message)
		{
		}
	}
}