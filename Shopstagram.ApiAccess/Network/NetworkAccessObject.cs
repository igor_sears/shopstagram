﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Web;

namespace Shopstagram.ApiAccess.Network
{

	public interface INetworkAccessObject
	{
		string Fetch(string url);
	}

	public class NetworkAccessObject : INetworkAccessObject
	{
		public string Fetch(string url)
		{
			try
			{
				Debug.WriteLine("Downloading from " + url);
				var request = WebRequest.Create(url);
				var response = request.GetResponse();

				var responseStream = response.GetResponseStream();

				if (responseStream == null) return null;
				var reader = new StreamReader(responseStream);
				var res = reader.ReadToEnd();

				Debug.WriteLine("Response: " + res);

				return res;
			}
			catch (WebException e)
			{
				switch (e.Status)
				{
					case WebExceptionStatus.ConnectFailure:
					case WebExceptionStatus.ConnectionClosed:
						throw new ConnectionClosedException("Connection error");
					case WebExceptionStatus.Timeout:
						throw new ConnectionClosedException("Timeout");
					case WebExceptionStatus.ReceiveFailure:
						throw new ConnectionClosedException("Receive failure");
					case WebExceptionStatus.ProtocolError:
						switch (((HttpWebResponse)e.Response).StatusCode)
						{
							case HttpStatusCode.NotFound:
								throw new ConnectionClosedException("404");
							case HttpStatusCode.Unauthorized:
								throw new ConnectionClosedException("401");
							case HttpStatusCode.BadRequest:
								throw new ConnectionClosedException("400");
						}
						break;
				}
				throw new Exception("Unknown exception");
			}

		}

	}

}
