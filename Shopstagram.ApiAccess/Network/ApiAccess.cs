﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Castle.Core.Internal;
using Shopstagram.ApiAccess.Dto;
using Shopstagram.ApiAccess.Providers;

namespace Shopstagram.ApiAccess.Network
{

    public interface IApiAccess
    {
        T Get<T>(string url, object parameters = null) where T : IDto;
	    IList<T> GetList<T>(string url, object parameters = null) where T : IDto;
    }

    public class ApiAccess : IApiAccess
    {
        private readonly INetworkAccessObject _networkAccess;
        private readonly IDtoMapper _dtoMapper;
        private readonly ITokenProvider _tokenProvider;
        private readonly IHashCodeProvider _hashCodeProvider;
        private readonly IConfigurationProvider _configurationProvider;

        public ApiAccess(
            INetworkAccessObject networkAccess, 
            IDtoMapper dtoMapper, 
            ITokenProvider tokenProvider, 
            IHashCodeProvider hashCodeProvider, 
            IConfigurationProvider configurationProvider)
        {
            _networkAccess = networkAccess;
            _dtoMapper = dtoMapper;
            _tokenProvider = tokenProvider;
            _hashCodeProvider = hashCodeProvider;
            _configurationProvider = configurationProvider;
        }

        public T Get<T>(string url, object parameters) where T : IDto
        {
            return _dtoMapper.MapTo<T>(GetData(url, parameters));
        }

		public IList<T> GetList<T>(string url, object parameters) where T : IDto
		{
			return _dtoMapper.MapToList<T>(GetData(url, parameters));
		}

        private string AddAuthParams(string url)
        {
            return url + "?token=" + _tokenProvider.Token + "&hash=" + _hashCodeProvider.GetHash();
        }

		private string GetData(string url, object parameters)
        {
			var queryParams = "";

			if (parameters == null)
				return _networkAccess.Fetch(AddAuthParams(_configurationProvider.BaseUrl + url) + queryParams);

			var type = parameters.GetType();
			type.GetProperties().ForEach(p =>
			{
				queryParams += "&";
				var value = p.GetValue(parameters, null);
				var name = p.Name;
				queryParams += name;
				queryParams += "=";
				queryParams += ParametersFormatter.FormatParameterWithComma((Collection<long>)value);
			});

			return _networkAccess.Fetch(AddAuthParams(_configurationProvider.BaseUrl + url) + queryParams);
        }
    }
}