﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Shopstagram.ApiAccess.Dto
{
    public interface IDtoMapper
    {
        T MapTo<T>(string source) where T : IDto;
        IList<T> MapToList<T>(string source) where T : IDto;
    }

    public class DtoMapper : IDtoMapper
    {
        public T MapTo<T>(string source) where T : IDto
        {
            return JsonConvert.DeserializeObject<T>(source);
        }

        public IList<T> MapToList<T>(string source) where T : IDto
        {
            return JsonConvert.DeserializeObject<List<T>>(source);
        }

    }
}
