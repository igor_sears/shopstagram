﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopstagram.ApiAccess.Dto
{
    public class CatalogDto : IDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long OwnerId { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string Url { get; set; }

        public string CoverImageUrl { get; set; }

        public string Privacy { get; set; }

        public Collection<CatalogProductDto> Items { get; set; }

        public string Type { get; set; }

        public int Likes { get; set; }

        public int Followers { get; set; }

        public override string ToString()
        {
            return string.Format("\nCatalogDto\nFollowers: {0}, Type: {1}, Likes: {2}, Items: {3}, Privacy: {4}, CoverImageUrl: {5}, Url: {6}, UpdatedDate: {7}, OwnerId: {8}, CreateDate: {9}, Description: {10}, Name: {11}, Id: {12}\n", Followers, Type, Likes, Items, Privacy, CoverImageUrl, Url, UpdatedDate, OwnerId, CreateDate, Description, Name, Id);
        }
    }

    public class CatalogProductDto
    {
        public DateTime AddedOn { get; set; }

        public long Id { get; set; }

        public override string ToString()
        {
            return string.Format("\nCatalogProductDto\nAddedOn: {0}, Id: {1}", AddedOn, Id);
        }
    }
}
