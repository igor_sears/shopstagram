﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopstagram.ApiAccess.Dto
{
    public class ProductDto : IDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ProductUrl { get; set; }
        public string CategoryName { get; set; }
        public string Source { get; set; }
        public string Sin { get; set; }
        public string ItemId { get; set; }

        public override string ToString()
        {
            return string.Format("\nProductDto\nId: {0}, Name: {1}, Description: {2}, ImageUrl: {3}, ProductUrl: {4}, CategoryName: {5}, Source: {6}, Sin: {7}, ItemId: {8}", Id, Name, Description, ImageUrl, ProductUrl, CategoryName, Source, Sin, ItemId);
        }
    }
}
