﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopstagram.ApiAccess.Dto
{
    public class UserDto : IDto
    {
        public long Id { get; set; }

        public string Privacy { get; set; }

        public string VipLevel { get; set; }

        public uint FacebookUserId { get; set; }

        public string Email { get; set; }

        public DateTime RegistrationDate { get; set; }

        public string ProfileUrl { get; set; }

        public long SearsId { get; set; }

        public string SywrMemberNumber { get; set; }

        public string ImageUrl { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("\nUserDto\nId: {0}, Privacy: {1}, VipLevel: {2}, FacebookUserId: {3}, Email: {4}, RegistrationDate: {5}, ProfileUrl: {6}," +
                                 " SearsId: {7}, SywrMemberNumber: {8}, ImageUrl: {9}, Name: {10}", Id, Privacy, VipLevel, FacebookUserId, Email, 
                                 RegistrationDate, ProfileUrl, SearsId, SywrMemberNumber, ImageUrl, Name);
        }
    }
}
