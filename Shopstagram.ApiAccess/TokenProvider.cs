﻿namespace Shopstagram.ApiAccess
{
    public interface ITokenProvider
    {
        string Token { get; set; } 
    }

    public class TokenProvider : ITokenProvider
    {
        public string Token { get; set; }
    }
}