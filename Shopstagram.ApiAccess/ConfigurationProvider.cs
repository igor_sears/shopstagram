﻿using System.Configuration;

namespace Shopstagram.ApiAccess
{
	public interface IConfigurationProvider
	{
		string AppId { get; }
		string AppSecret { get; }
		string BaseUrl { get; }
	}

	public class ConfigurationProvider : IConfigurationProvider
	{
		public string AppId
		{
			get { return ConfigurationManager.AppSettings["platform:app:id"]; }
		}

		public string AppSecret
		{
			get { return ConfigurationManager.AppSettings["platform:app:secret"]; }
		}

		public string BaseUrl
		{
			get { return ConfigurationManager.AppSettings["platform:app:baseurl"]; }
		}
	}
}