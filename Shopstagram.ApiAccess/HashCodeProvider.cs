﻿using System.Security.Cryptography;
using System.Text;
using Shopstagram.ApiAccess.Extensions;

namespace Shopstagram.ApiAccess
{

    public interface IHashCodeProvider
    {
        string GetHash();
    }

    public class HashCodeProvider : IHashCodeProvider
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IConfigurationProvider _configurationProvider;

        public HashCodeProvider(ITokenProvider tokenProvider, IConfigurationProvider configurationProvider)
        {
            _tokenProvider = tokenProvider;
            _configurationProvider = configurationProvider;
        }

        public string GetHash()
        {
            var saltedWithSecret = Encoding.UTF8.GetBytes(_tokenProvider.Token + _configurationProvider.AppSecret);
            return SHA256.Create().ComputeHash(saltedWithSecret).ToHexString().ToLower();
        }
    }
}