﻿using Shopstagram.ApiAccess.Dto;
using Shopstagram.ApiAccess.Network;

namespace Shopstagram.ApiAccess.Providers
{
    public interface ICurrentUserDtoApi
    {
        UserDto GetCurrentUserDto();
    }

	public class CurrentUserDtoApi : ICurrentUserDtoApi
    {
        private readonly IApiAccess _apiAccess;

        public CurrentUserDtoApi(IApiAccess apiAccess)
        {
            _apiAccess = apiAccess;
        }

        public UserDto GetCurrentUserDto()
        {
            return _apiAccess.Get<UserDto>("/users/current/");
        }

    }
}