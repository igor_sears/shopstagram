﻿using System;
using System.Collections.Generic;

namespace Shopstagram.ApiAccess.Providers
{
    public class ParametersFormatter
    {
        public static string FormatParameterWithComma<T>(IEnumerable<T> parameters)
        {
	        if (parameters == null) throw new NullReferenceException();
			return String.Join(",",parameters);
        } 
    }
}