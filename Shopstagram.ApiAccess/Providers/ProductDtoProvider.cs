﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Shopstagram.ApiAccess.Dto;
using Shopstagram.ApiAccess.Network;

namespace Shopstagram.ApiAccess.Providers
{
    public interface IProductDtoApi
    {
        IList<ProductDto> GetProducts(Collection<long> ids);
    }

    public class ProductDtoApi : IProductDtoApi
    {
        private readonly IApiAccess _apiAccess;

        public ProductDtoApi(IApiAccess apiAccess)
        {
            _apiAccess = apiAccess;
        }

        public IList<ProductDto> GetProducts(Collection<long> _ids)
        {
            return _apiAccess.GetList<ProductDto>("/products/get", new {ids = _ids});
        }
    }
}