﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Shopstagram.ApiAccess.Dto;
using Shopstagram.ApiAccess.Network;

namespace Shopstagram.ApiAccess.Providers
{
    public interface IOwnCatalogDtoApi
    {
        IList<CatalogDto> GetOwnedCatalogDto();
    }

	public class OwnCatalogDtoApi : IOwnCatalogDtoApi
    {
        private readonly IApiAccess _apiAccess;
        private readonly ICurrentUserDtoApi _currentUserDtoApi;

        public OwnCatalogDtoApi(IApiAccess apiAccess, ICurrentUserDtoApi currentUserDtoApi)
        {
            _apiAccess = apiAccess;
            _currentUserDtoApi = currentUserDtoApi;
        }

        public IList<CatalogDto> GetOwnedCatalogDto()
        {
            var userDto = _currentUserDtoApi.GetCurrentUserDto();
            Debug.WriteLine(userDto);

			return _apiAccess.GetList<CatalogDto>("/catalogs/own/get/", new {ids = new Collection<long> { userDto.Id }});
        }
    }
}