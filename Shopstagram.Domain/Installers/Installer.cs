﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Shopstagram.ApiAccess.Dto;

namespace Shopstagram.Domain.Installers
{
    public class Installer : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ICategoryProvider>().ImplementedBy<CategoryProvider>().LifestyleTransient());
            container.Register(Component.For<IReviewedProductProvider>().ImplementedBy<ReviewedProductProvider>().LifestyleTransient());
            container.Register(Component.For<IImageHandler>().ImplementedBy<ImageHandler>().LifestyleTransient());
            container.Register(Component.For<IUserProvider>().ImplementedBy<UserProvider>().LifestyleTransient());
        }
    }
}
