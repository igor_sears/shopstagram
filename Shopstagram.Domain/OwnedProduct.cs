﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shopstagram.ApiAccess.Dto;

namespace Shopstagram.Domain
{
    public class OwnedProduct
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ProductUrl { get; set; }
        public string CategoryName { get; set; }

        public override string ToString()
        {
            return string.Format("\nOwnedProduct\nId: {0}, ProductName: {1}, Description: {2}, ImageUrl: {3}, ProductUrl: {4}, CategoryName: {5}", Id, ProductName, Description, ImageUrl, ProductUrl, CategoryName);
        }
    }

    public static class OwnedProductDtoExtensions
    {
        public static OwnedProduct MapToItem(this ProductDto s)
        {
            return new OwnedProduct
            {
                Id = s.Id,
                ProductName = s.Name,
                Description = s.Description,
                ImageUrl = s.ImageUrl,
                ProductUrl = s.ProductUrl,
                CategoryName = s.CategoryName
            };
        }
    }
}
