﻿using System.Collections.Generic;
using System.Linq;
using Shopstagram.Data;

namespace Shopstagram.Domain
{

    public interface IReviewedProductProvider
    {
        IEnumerable<ReviewdItem> GetAllReviewdItems();
        ReviewdItem Get(int id);
    }

    class ReviewedProductProvider : IReviewedProductProvider
    {
        private readonly IStorage _storage;

        public ReviewedProductProvider(IStorage storage)
        {
            _storage = storage;
        }

        public IEnumerable<ReviewdItem> GetAllReviewdItems()
        {
            var reviewdItemDtos = _storage.GetAll();
            return reviewdItemDtos.Select(x => x.MapToItem());
        }

        public ReviewdItem Get(int id)
        {
            var reviewdItemDto = _storage.Get(id);
            return reviewdItemDto.MapToItem();
        }
    }
}
