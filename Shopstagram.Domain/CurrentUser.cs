﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shopstagram.ApiAccess.Dto;

namespace Shopstagram.Domain
{
    public class CurrentUser
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public string ProfileUrl { get; set; }

        public string ImageUrl { get; set; }

        public string Name { get; set; }
    }

    public static class UserDtoExtension
    {
        public static CurrentUser MapToItem(this UserDto s)
        {
            return new CurrentUser
            {
                Id = s.Id,
                Name = s.Name,
                Email = s.Email,
                ImageUrl = s.ImageUrl,
                ProfileUrl = s.ProfileUrl
            };
        }
    }
}
