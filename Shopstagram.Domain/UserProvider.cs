﻿using Shopstagram.ApiAccess;
using Shopstagram.ApiAccess.Providers;

namespace Shopstagram.Domain
{
    public interface IUserProvider
    {
        CurrentUser GetCurrentUser();
    }

    class UserProvider : IUserProvider
    {
        private readonly ICurrentUserDtoApi _currentUserDtoApi;

        public UserProvider(ICurrentUserDtoApi currentUserDtoApi)
        {
            _currentUserDtoApi = currentUserDtoApi;
        }


        public CurrentUser GetCurrentUser()
        {
            var currentUserDto = _currentUserDtoApi.GetCurrentUserDto();
            return currentUserDto.MapToItem();
        }
    }
}
