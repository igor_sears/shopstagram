﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopstagram.Domain.Models
{
    public class ReviewedItemModel
    {
        public virtual long Id { get; set; }
        public virtual long ProductId { get; set; }
        public virtual string ProductCategory { get; set; }
        public virtual string ProductUrl { get; set; }
        public virtual string ProductImgUrl { get; set; }
        public virtual string UploadedUrl { get; set; }
        public virtual long UploadingUserId { get; set; }
    }

    public static class ReviewedItemExtension
    {
        public static ReviewedItemModel ToModel(this ReviewdItem source)
        {
            return new ReviewedItemModel
            {
                Id = source.Id,
                ProductId = source.ProductId,
                ProductUrl = source.ProductUrl,
                ProductImgUrl = source.ProductImgUrl,
                UploadedUrl = source.UploadedUrl,
                UploadingUserId = source.UploadingUserId,
                ProductCategory = source.ProductCategory
            };
        }
    }
}
