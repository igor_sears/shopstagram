﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopstagram.Domain.Models
{
    public class ProductModel
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ProductUrl { get; set; }
        public string CategoryName { get; set; }
    }

    public static class OwnedProductExtension
    {
        public static ProductModel ToModel(this OwnedProduct s)
        {
            return new ProductModel
            {
                Id = s.Id,
                CategoryName = s.CategoryName,
                ProductUrl = s.ProductUrl,
                ProductName = s.ProductName,
                Description = s.Description,
                ImageUrl = s.ImageUrl
            };
        }
    }
}
