﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shopstagram.Data.ClassMaps;

namespace Shopstagram.Domain
{
    public class ReviewdItem
    {
        public virtual long Id { get; set; }
        public virtual long ProductId { get; set; }
        public virtual string ProductCategory { get; set; }
        public virtual string ProductUrl { get; set; }
        public virtual string ProductImgUrl { get; set; }
        public virtual string UploadedUrl { get; set; }
        public virtual long UploadingUserId { get; set; }

        public override string ToString()
        {
            return string.Format("\nReviewdItem\nId: {0}, ProductId: {1}, ProductCategory: {2}, ProductUrl: {3}, ProductImgUrl: {4}, UploadedUrl: {5}, UploadingUserId: {6}", Id, ProductId, ProductCategory, ProductUrl, ProductImgUrl, UploadedUrl, UploadingUserId);
        }
    }

    public static class ReviewedDtoExtensions
    {
        public static ReviewdItem MapToItem(this ReviewdItemDto source)
        {
            return new ReviewdItem
            {
                Id = source.Id,
                ProductId = source.Product_id,
                ProductImgUrl = source.Product_img_url,
                ProductUrl = source.Product_url,
                ProductCategory = source.Product_category,
                UploadedUrl = source.Uploaded_url,
                UploadingUserId = source.Uploading_user_id
            };
        }
    }
}
