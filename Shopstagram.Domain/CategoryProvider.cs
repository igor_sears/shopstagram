﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Castle.Core.Internal;
using Shopstagram.ApiAccess.Providers;

namespace Shopstagram.Domain
{

    public interface ICategoryProvider
    {
        IEnumerable<OwnedProduct> GetOwnedProducts();
    }

    class CategoryProvider : ICategoryProvider
    {

        private readonly IProductDtoApi _productDtoApi;
		private readonly IOwnCatalogDtoApi _catalogDtoApi;

        public CategoryProvider( IProductDtoApi productDtoApi, IOwnCatalogDtoApi catalogDtoApi)
        {
	        _productDtoApi = productDtoApi;
	        _catalogDtoApi = catalogDtoApi;
        }

	    public IEnumerable<OwnedProduct> GetOwnedProducts()
        {
			var ownedCatalogDto = _catalogDtoApi.GetOwnedCatalogDto();
	        var ids = ownedCatalogDto.SelectMany(x => x.Items).Select(y => y.Id).ToList();
            return _productDtoApi.GetProducts(new Collection<long>(ids)).Select(x => x.MapToItem());
        }   
    }
}
