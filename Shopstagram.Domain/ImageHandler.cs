﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shopstagram.Data;
using System.Web;
using Shopstagram.Data.ClassMaps;
using Shopstagram.Domain.Models;

namespace Shopstagram.Domain
{
    public interface IImageHandler
    {
        bool SaveImage(HttpPostedFileBase file, String path, ProductModel product, CurrentUser user);
    }

    public class ImageHandler : IImageHandler
    {
        private readonly IStorage _storage;
        private readonly IImageStorage _imageStorage;

        public ImageHandler(IStorage storage, IImageStorage imageStorage)
        {
            _storage = storage;
            _imageStorage = imageStorage;
        }

        public bool SaveImage(HttpPostedFileBase file, String path, ProductModel product, CurrentUser user)
        {
            var metaDataDto = _imageStorage.SaveImage(file,path);
            if (metaDataDto == null) return false;
            _storage.Save(new ReviewdItemDto
            {
                Product_category = product.CategoryName,
                Product_id = product.Id, 
                Product_img_url = product.ImageUrl, 
                Product_url = product.ProductUrl,
                Uploaded_url = "../../static/saved/" + file.FileName,
                Uploading_user_id = user.Id
            }); 
            return true;
        }
    }
}
