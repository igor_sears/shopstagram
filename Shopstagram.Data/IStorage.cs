﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shopstagram.Data.ClassMaps;

namespace Shopstagram.Data
{
    public interface IStorage
    {
        ReviewdItemDto Get(int id);
        IList<ReviewdItemDto> GetAll();
        bool Save(ReviewdItemDto data);
        IList<ReviewdItemDto> GetAll(int userId);
    }
}
