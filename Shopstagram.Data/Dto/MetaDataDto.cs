﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopstagram.Data.Dto
{
    public class MetaDataDto
    {
        public string Url { get; set; }
    }
}
