﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Shopstagram.Data.Installers
{
    public class StorageInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IStorage>().ImplementedBy<ReviewsLocalStorage>().LifestyleSingleton());
            container.Register(
                Component.For<IImageStorage>().ImplementedBy<ImageStorage>().LifestyleSingleton());
        }
    }
}
