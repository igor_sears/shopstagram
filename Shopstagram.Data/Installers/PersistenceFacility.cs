﻿using Castle.Core.Internal;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using Shopstagram.Data.ClassMaps;

namespace Shopstagram.Data.Installers
{
    public class PersistenceFacility : AbstractFacility
    {
        protected override void Init()
        {
            var config = BuildDatabaseConfiguration();

            Kernel.Register(
            Component.For<ISessionFactory>()
                .UsingFactoryMethod(_ => config.BuildSessionFactory()),
            Component.For<ISession>()
                .UsingFactoryMethod(k => k.Resolve<ISessionFactory>().OpenSession()));
        }

        private Configuration BuildDatabaseConfiguration()
        {
            return Fluently.Configure()
            .Database(SetupDatabase)
            .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ReviewdItemDtoMap>())
            .ExposeConfiguration(ConfigurePersistence)
            .BuildConfiguration();
        }

        protected virtual void ConfigurePersistence(Configuration config)
        {
            SchemaMetadataUpdater.QuoteTableAndColumns(config);
        }

        protected virtual IPersistenceConfigurer SetupDatabase()
        {
            return MySQLConfiguration.Standard
                            .ConnectionString(x => x.Server("localhost").Database("shopstagram").Username("root").Password("MF12345"));
        }

        protected virtual AutoPersistenceModel CreateMappingModel()
        {
            return AutoMap.Assembly(typeof(ReviewdItemDtoMap).Assembly)
                .Where(IsDomainEntity);
        }

        protected virtual bool IsDomainEntity(Type t)
        {
            return typeof(ReviewdItemDtoMap).IsAssignableFrom(t);
        }

    }
}
