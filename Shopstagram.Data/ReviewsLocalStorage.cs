﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NHibernate;
using NHibernate.Criterion;
using Shopstagram.Data.ClassMaps;

namespace Shopstagram.Data
{
    class ReviewsLocalStorage : IStorage
    {
        private readonly ISession _session;

        public ReviewsLocalStorage(ISession session)
        {
            _session = session;
        }

        public ReviewdItemDto Get(int id)
        {
            var uniqueResult =
                _session.CreateCriteria<ReviewdItemDto>().Add(Restrictions.IdEq(id)).UniqueResult<ReviewdItemDto>();
            return uniqueResult;
        }

        public IList<ReviewdItemDto> GetAll()
        {
            return _session.CreateCriteria<ReviewdItemDto>().List<ReviewdItemDto>();
        }

        public IList<ReviewdItemDto> GetAll(int userId)
        {
            return
                _session.CreateCriteria<ReviewdItemDto>()
                    .Add(Restrictions.Eq("upoading_user_id", userId))
                    .List<ReviewdItemDto>();
        }

        public bool Save(ReviewdItemDto data)
        {
            try
            {
                _session.SaveOrUpdate(data);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }
    }
}
