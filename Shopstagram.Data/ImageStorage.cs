﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Shopstagram.Data.Dto;

namespace Shopstagram.Data
{

    public interface IImageStorage
    {
        MetaDataDto SaveImage(HttpPostedFileBase file, string path);
    }

    class ImageStorage : IImageStorage
    {
        public MetaDataDto SaveImage(HttpPostedFileBase file, String path)
        {
            try
            {
                System.IO.Directory.CreateDirectory(path);
                if ((file == null) || (file.ContentLength <= 0) || string.IsNullOrEmpty(file.FileName)) return null;
                var fileName = file.FileName;
                file.SaveAs(path + fileName);
                return new MetaDataDto { Url = path + fileName };
            }
            catch (Exception)
            {
                return null;
            }
            
        }
    }
}
