﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Shopstagram.Data.ClassMaps
{
    public class ReviewdItemDto
    {
        public virtual long Id { get; set; }
        public virtual long Product_id { get; set; }
        public virtual string Product_category { get; set; }
        public virtual string Product_url { get; set; }
        public virtual string Product_img_url { get; set; }
        public virtual string Uploaded_url { get; set; }
        public virtual long Uploading_user_id { get; set; }
    }

    class ReviewdItemDtoMap : ClassMap<ReviewdItemDto>
    {
         private const string TableName = "reviewd_products";

        public ReviewdItemDtoMap()
        {
            Table(TableName);
            Id(x => x.Id);
            Map(x => x.Product_id);
            Map(x => x.Product_category);
            Map(x => x.Product_url);
            Map(x => x.Product_img_url);
            Map(x => x.Uploaded_url);
            Map(x => x.Uploading_user_id);
        }
    }
}
