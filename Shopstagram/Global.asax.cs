﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Shopstagram.ApiAccess;
using Shopstagram.ApiAccess.Dto;
using Shopstagram.Common;
using Shopstagram.Data;
using Shopstagram.Data.ClassMaps;
using Shopstagram.Domain;
using Shopstagram.plumbing;

namespace Shopstagram
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
			IWindsorContainer container = new WindsorContainer();
			Bootstrap(container);
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
			GlobalFilters.Filters.Add(new TokenInterceptor(container.Resolve<ITokenProvider>()));
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteTable.Routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

		private static void Bootstrap(IWindsorContainer container)
        {
            container.Install(FromAssembly.Containing<IDto>()); //API Access
            container.Install(FromAssembly.Containing<ReviewdItemDto>()); //Persistence
            container.Install(FromAssembly.Containing<ImageHandler>()); //Domain
            container.Install(FromAssembly.This()); //UI

            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }

    }
}