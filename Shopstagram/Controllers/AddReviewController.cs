﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Shopstagram.Domain;
using Shopstagram.Domain.Models;

namespace Shopstagram.Controllers
{
    public class AddReviewController : Controller
    {
        //
        // GET: /AddReview/

        private readonly ICategoryProvider _categoryProvider;

        public AddReviewController(ICategoryProvider categoryProvider)
        {
            _categoryProvider = categoryProvider;
        }


        public ActionResult AddReview(string token)
        {
            var ownedProducts = _categoryProvider.GetOwnedProducts();
            var modelList = ownedProducts.Select(x => x.ToModel()).ToList();
            TempData["productModel"] = modelList;
            return View(new AddReviewModelWrapper(modelList, token));
        }

        public class AddReviewModelWrapper
        {
            public IList<ProductModel> List { get; set; }
	        public string Token { get; set; }

            public AddReviewModelWrapper(IList<ProductModel> list, string token)
            {
                List = list;
	            Token = token;
            }
        }

    }
}
