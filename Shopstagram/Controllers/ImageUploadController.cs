﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shopstagram.ApiAccess;
using Shopstagram.Domain;
using Shopstagram.Domain.Models;

namespace Shopstagram.Controllers
{
    public class ImageUploadController : Controller
    {
        //
        // GET: /FileUpload/

        private readonly IImageHandler _imageHandler;
        private readonly IUserProvider _userProvider;
        private readonly ITokenProvider _tokenProvider;

        public ImageUploadController(IImageHandler imageHandler,IUserProvider userProvider, ITokenProvider tokenProvider)
        {
            _imageHandler = imageHandler;
            _userProvider = userProvider;
            _tokenProvider = tokenProvider;
        }

        public ActionResult Upload(long id, string token)
        {
            TempData["id"] = id;
            return View();
        }

        public ActionResult Save(FormCollection formCollection)
        {
            if (TempData["productModel"] != null && TempData["id"] != null)
            {
                var products = (List<ProductModel>)TempData["productModel"];
                var id = (long) TempData["id"];

                var product = products.First(x => x.Id == id);

                if (Request != null)
                {
                    var currentUser = _userProvider.GetCurrentUser();
                    HttpPostedFileBase file = Request.Files["UploadedFile"];
                    _imageHandler.SaveImage(file, Server.MapPath("/static/saved/"), product, currentUser);
                }
                return RedirectToAction("Index", "Landing", new {token = _tokenProvider.Token});
            }

            return View("Error");

        }

       

    }
}
