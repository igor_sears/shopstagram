﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shopstagram.ApiAccess;
using Shopstagram.Common;
using Shopstagram.Domain;
using Shopstagram.Domain.Models;

namespace Shopstagram.Controllers
{
    public class LandingController : Controller
    {

        private readonly IReviewedProductProvider _reviewedProductProvider;
		private readonly IConfigurationProvider _configurationProvider;

        public LandingController(IReviewedProductProvider reviewedProductProvider, IConfigurationProvider configurationProvider)
        {
	        _reviewedProductProvider = reviewedProductProvider;
	        _configurationProvider = configurationProvider;
        }

	    //
        // GET: /Landing/

        public ActionResult Index(string token)
        {
            var allReviewdItems = _reviewedProductProvider.GetAllReviewdItems();
			return View(new LandingModelWrapper(_configurationProvider) { ItemList = allReviewdItems.Select(x => x.ToModel()).ToList(), Token = token});
        }

        public class LandingModelWrapper
        {
	        private readonly IConfigurationProvider _configurationProvider;

	        public LandingModelWrapper(IConfigurationProvider configurationProvider)
	        {
		        _configurationProvider = configurationProvider;
	        }

	        public IList<ReviewedItemModel> ItemList { get; set; }
	        public string Token { get; set; }
			public string AppId {
				get { return _configurationProvider.AppId; }
			}
	        public string BaseUrl { 
				get { return _configurationProvider.BaseUrl; }
	        }
        }

    }
}
