﻿using System;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;
using Castle.Core.Internal;
using Shopstagram.ApiAccess;

namespace Shopstagram.Common
{
    public class TokenInterceptor : IActionFilter
    {
        private readonly ITokenProvider _tokenProvider;

	    public TokenInterceptor(ITokenProvider tokenProvider)
	    {
		    _tokenProvider = tokenProvider;
	    }

	    public void OnActionExecuting(ActionExecutingContext filterContext)
	    {
		    var token = filterContext.HttpContext.Request.QueryString.Get("token");
			var cookie = HttpContext.Current.Request.Cookies["Token"];
		    if (token.IsNullOrEmpty())
		    {
			    if (cookie != null)
			    {
				    token = cookie.Value;
			    }
		    }

		    if (cookie == null)
		    {
			    cookie = new HttpCookie("Token",token);
		    }
		    else
		    {
			    cookie.Value = token;
		    }
		    cookie.Expires = DateTime.Now.AddYears(1);
			HttpContext.Current.Response.Cookies.Add(cookie);
		    _tokenProvider.Token = token;
		    Debug.WriteLine(_tokenProvider.Token == null ? "no token to snap" : "snapped a token");
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }
    }
}