﻿using Shopstagram.ApiAccess.Dto;

namespace Tests.Mock
{
    public class MockDto : IDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}