﻿using Shopstagram.ApiAccess;

namespace Tests.Mock
{
    public class MockHashCodeProvider : IHashCodeProvider
    {
	    public string Hash { get; set; }
        public string GetHash()
        {
            return Hash;
        }
    }
}