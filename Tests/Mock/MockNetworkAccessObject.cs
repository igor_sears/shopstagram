﻿using Shopstagram.ApiAccess.Network;

namespace Tests.Mock
{
    public class MockNetworkAccessObject : INetworkAccessObject
    {

	    public string ReturnValue { get; set; }
        public string Fetch(string url)
        {
            return ReturnValue;
        }
    }
}