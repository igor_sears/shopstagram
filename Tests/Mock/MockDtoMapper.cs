﻿using System.Collections.Generic;
using Shopstagram.ApiAccess.Dto;

namespace Tests.Mock
{
    public class MockDtoMapper : IDtoMapper
    {
        public T MapTo<T>(string source) where T : IDto
        {
            throw new System.NotImplementedException();
        }

        public IList<T> MapToList<T>(string source) where T : IDto
        {
            throw new System.NotImplementedException();
        }

        public string MapFrom(IDto source)
        {
            throw new System.NotImplementedException();
        }
    }
}