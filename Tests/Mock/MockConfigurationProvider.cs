﻿using Shopstagram.ApiAccess;

namespace Tests.Mock
{
    public class MockConfigurationProvider : IConfigurationProvider
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string BaseUrl { get; set; }
    }


}