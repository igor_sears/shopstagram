﻿using Shopstagram.ApiAccess;

namespace Tests.Mock
{
    public class MockTokenProvider : ITokenProvider
    {
        public string Token { get; set; }
    }
}