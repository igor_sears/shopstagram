﻿using System.Linq;
using NUnit.Framework;
using Shopstagram.ApiAccess.Dto;
using Shopstagram.ApiAccess.Network;
using Tests.Mock;

namespace Tests
{
    [TestFixture]
    public class ApiAccessTest
    {
        private IApiAccess _apiAccess;
	    private MockNetworkAccessObject _mockNetworkAccessObject;
	    private DtoMapper _dtoMapper;
	    private MockTokenProvider _mockTokenProvider;
	    private MockHashCodeProvider _mockHashCodeProvider;
	    private MockConfigurationProvider _mockConfigurationProvider;

        [SetUp]
        public void Setup()
        {
				_mockNetworkAccessObject = new MockNetworkAccessObject();
				_dtoMapper = new DtoMapper();
				_mockTokenProvider = new MockTokenProvider();
				_mockHashCodeProvider = new MockHashCodeProvider();
				_mockConfigurationProvider = new MockConfigurationProvider();
        }

        [Test]
        public void Get_SomeUrlMockDto_ReturnsValidMockDto()
        {
			SetNetworkResult("{\"id\":123,\"name\":\"TEST\"}");
			SetToken("tokkkken");
			SetHash("hashhhhh");
			SetAppConfigs("1233212","appSecret","www.basde.url");

			_apiAccess = new ApiAccess(_mockNetworkAccessObject,_dtoMapper,_mockTokenProvider,_mockHashCodeProvider,_mockConfigurationProvider);

            var mockDto = _apiAccess.Get<MockDto>("/some/endpoit");

            Assert.That(mockDto, Is.Not.Null);
            Assert.That(mockDto.Id, Is.EqualTo(123));
            Assert.That(mockDto.Name, Is.EqualTo("TEST"));
        }

		[Test]
		public void Get_SomeUrlMockDtoList_ReturnsValidMockDtoList()
		{
			SetNetworkResult("[{\"id\":123,\"name\":\"TEST\"},{\"id\":123,\"name\":\"TEST\"},{\"id\":123,\"name\":\"TEST\"},{\"id\":123,\"name\":\"TEST\"}]");
			SetToken("tokkkken");
			SetHash("hashhhhh");
			SetAppConfigs("1233212", "appSecret", "www.basde.url");

			_apiAccess = new ApiAccess(_mockNetworkAccessObject, _dtoMapper, _mockTokenProvider, _mockHashCodeProvider, _mockConfigurationProvider);

			var mockDto = _apiAccess.GetList<MockDto>("/some/endpoit/list");

			Assert.That(mockDto, Is.Not.Null);
			Assert.That(mockDto.Count, Is.EqualTo(4));
			Assert.That(mockDto.Last().Id, Is.EqualTo(123));
			Assert.That(mockDto.Last().Name, Is.EqualTo("TEST"));
		}

	    private void SetNetworkResult(string result)
	    {
		    _mockNetworkAccessObject.ReturnValue = result;
	    }

	    private void SetToken(string token)
	    {
		    _mockTokenProvider.Token = token;
	    }

	    private void SetHash(string hash)
	    {
		    _mockHashCodeProvider.Hash = hash;
	    }

	    private void SetAppConfigs(string appId, string appSecret, string baseUrl)
	    {
		    _mockConfigurationProvider.AppId = appId;
		    _mockConfigurationProvider.AppSecret = appSecret;
		    _mockConfigurationProvider.BaseUrl = baseUrl;
	    }
    }
}