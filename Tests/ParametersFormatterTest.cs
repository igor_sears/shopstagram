﻿using System;
using System.Collections.ObjectModel;
using Castle.Core.Internal;
using NUnit.Framework;
using Shopstagram.ApiAccess.Providers;

namespace Tests
{
	[TestFixture]
	public class ParametersFormatterTest
	{

		[Test]
		public void FormatParameterWithComma_NullParams_ThrowsNullRefException()
		{
			Assert.Throws<NullReferenceException>(() => ParametersFormatter.FormatParameterWithComma<long>(null));
		}

		[Test]
		public void FormatParameterWithComma_ParamsCollectionOfThree_RetunsStrigWithCommasBetweenParams()
		{
			var result = ParametersFormatter.FormatParameterWithComma(new Collection<long> {1,2,3,4});

			Assert.That(result, Is.EqualTo("1,2,3,4"));
		}

		[Test]
		public void FormatParameterWithComma_ParamsCollectionSingleItem_RetunsStrigWithSingleItem()
		{
			var result = ParametersFormatter.FormatParameterWithComma(new Collection<long> { 1 });

			Assert.That(result, Is.EqualTo("1"));
		}

		[Test]
		public void FormatParameterWithComma_ParamsCollectionNoParams_RetunsEmptyString()
		{
			var result = ParametersFormatter.FormatParameterWithComma(new Collection<long> ());

			Assert.That(result, Is.Not.Null);
			Assert.That(result, Is.EqualTo(""));
			Assert.That(result.IsNullOrEmpty(), Is.True);
		}
	}
}