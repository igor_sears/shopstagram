﻿using NUnit.Framework;
using Shopstagram.ApiAccess;
using Tests.Mock;

namespace Tests
{
    [TestFixture]
    public class HashCodeProviderTest
    {
        private HashCodeProvider _hashCodeProvider;
        private MockTokenProvider _mockTokenProvider;
        private MockConfigurationProvider _mockConfigurationProvider;

        [SetUp]
        public void Setup()
        {
            _mockTokenProvider = new MockTokenProvider();
            _mockConfigurationProvider = new MockConfigurationProvider();
            _hashCodeProvider = new HashCodeProvider(_mockTokenProvider, _mockConfigurationProvider);
        }

        [Test]
        public void GetHash_predefinedTokenPredefinedHash_ReturnPredefinedHash()
        {
            SetToken("asdqwezxc");
            SetSecret("123456789");

            var hash = _hashCodeProvider.GetHash();

            Assert.That(hash, Is.EqualTo("a3b27bba65287cb4eb65fb6132d52d8d10542fffa647492236412559d57752e9"));
        }

        private void SetSecret(string secret)
        {
            _mockConfigurationProvider.AppSecret = secret;
        }

        private void SetToken(string token)
        {
            _mockTokenProvider.Token = token;
        }
    }
}