﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NUnit.Framework;
using Shopstagram.ApiAccess.Dto;
using Tests.Mock;

namespace Tests
{
    [TestFixture]
    public class DtoMapperTest
    {
        private MockDto _dto;
        private IDtoMapper _dtoMapper;

        [SetUp]
        public void Init()
        {
            _dto = new MockDto {Id = 123, Name = "TEST"};
            _dtoMapper = new DtoMapper();
        }


        [Test]
        public void MapTo_MockDtoStringValid_returnsMockDto()
        {
            var mockDto = _dtoMapper.MapTo<MockDto>("{\"id\":123,\"name\":\"TEST\"}");

            Assert.That(mockDto, Is.Not.Null);
            Assert.That(mockDto.Id, Is.EqualTo(123));
            Assert.That(mockDto.Name, Is.EqualTo("TEST"));
        }

        [Test]
        public void MapToList_MockDtoStringValid_returnsListOfMockDto()
        {
            var mockDtoList = _dtoMapper.MapToList<MockDto>("[{\"id\":123,\"name\":\"TEST\"},{\"id\":123,\"name\":\"TEST\"},{\"id\":123,\"name\":\"TEST\"},{\"id\":123,\"name\":\"TEST\"}]");

            Assert.That(mockDtoList, Is.Not.Null);
            Assert.That(mockDtoList.Count, Is.EqualTo(4));
            Assert.That(mockDtoList.First().Id, Is.EqualTo(123));
            Assert.That(mockDtoList.First().Name, Is.EqualTo("TEST"));
        }
    }
}